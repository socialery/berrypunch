import datetime;import re;import urllib;import json;from ircutils import format;import random

c=event.command;s=event.source
try:t=event.target
except:t="N/A"
p=' '.join(event.params)
if c not in ["PING"]:print "{0} <{1}@{2}> {3}".format(c,s,t,p).encode('ascii','replace')
if c=="INVITE":self.join_channel(p)


if event.command in ['PRIVMSG']:
 if "S"==event.source and event.message.startswith("<")and">"in event.message:
  event.message=event.message.split('> ',1)[1]
 c=event.message.split(' ')[0]
 try:p=event.message.split(' ',1)[1]
 except:p=''
 s=event.source
 t=event.target if event.target != self.nickname else event.source
 if "script" in s and event.message == '1':self.send_message(t,"no")

 if c=="!test":self.send_message(t,"[](/"+random.choice(["07","1b","4c","4d","adviceberry","bberryflip","berryflip","berrygasp","berryheart","berrynotwant","berryparty","berrypoker","berrysmug","bpcheers","bpdrunk","bpgross","bphappy","bphungover","bpmotherofgod","bpshrug","gw00","fruitaberry","bpwhy","bpwet","bpunchdrunk","bptear","lush","moar","offthewagon","punchdrunk","wmberry","themagicalcouch","ww21","z17","zz32"])+")")
 if c.lower() in ['!select'] and len(p)>0:self.send_message(t,'Select: '+random.choice(p.split(' ')))
 if c.lower() in ['!8ball']:
  ans=random.choice([(1,"It is certain"),(1,"It is decidedly so"),(1,"Without a doubt"),(1,"Yes - definitely"),(1,"You may rely on it"),(1,"As I see it, yes"),(1,"Most likely"),(1,"Outlook good"),(1,"Yes"),(1,"Signs point to yes"),(2,"Reply hazy, try again"),(2,"Ask again later"),(2,"Better not tell you now"),(2,"Cannot predict now"),(2,"Concentrate and ask again"),(3,"Don't count on it"),(3,"My reply is no"),(3,"My sources say no"),(3,"Outlook not so good"),(3,"Very doubtful")])
  if ans[0]==1:x=format.color('8Ball','0'+format.GREEN)
  if ans[0]==2:x=format.color('8Ball','0'+format.YELLOW)
  if ans[0]==3:x=format.color('8Ball','0'+format.RED)
  self.send_message(t,x+': '+ans[1])
 if c.lower() in ['~flip','!flip']:self.send_message(t,'Flip: '+random.choice(['Heads','Tails'])+'')
 if c.lower() in ['!t','!ts']:
  twi=format.color('t',format.AQUA)+':'
  jd=json.load(urllib.urlopen('http://search.twitter.com/search.json?q={q}&result_type=recent&include_entities=true&rpp=1'.format(q=urllib.quote_plus(p))))[u"results"][0]
  tuser=jd[u"from_user"].encode('utf-8','replace')
  ttext=jd[u"text"].encode('utf-8','replace')
  for x in jd[u"entities"][u"urls"]:ttext=ttext[:x[u"indices"][0]]+x[u"expanded_url"].encode('utf-8','replace')+ttext[x[u"indices"][1]:]
  self.send_message(t,twi+' '+tuser+': '+ttext)
 yt=format.color('ytt',format.RED)+':'
 enabled=True
 if enabled:
  ytmatch=re.compile("https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube\.com\S*[^\w\-\s])([\w\-]{11})(?=[^\w\-]|$)(?![?=&+%\w]*(?:['\"][^<>]*>|<\/a>))[?=&+%\w-]*",flags=re.I)
  matches=ytmatch.findall(event.message)
  for x in matches:
   try:
    jd=json.load(urllib.urlopen('http://gdata.youtube.com/feeds/api/videos/{id}?v=2&alt=jsonc'.format(id=x)))[u'data']
    self.send_message(t,yt+' {title} | {views} | {rating}% | {duration}'.format(title=jd[u'title'].encode('utf-8','replace'),views="{:,}".format(jd[u'viewCount']),rating=str(jd[u'rating']/5*100).split('.')[0],duration=str(datetime.timedelta(seconds=jd[u'duration']))))
   except:pass
 if c.lower() in ['!ytt']:
  try:
   jd=json.load(urllib.urlopen('https://gdata.youtube.com/feeds/api/videos?q={q}&max-results=1&v=2&alt=jsonc'.format(q=urllib.quote_plus(p))))[u'data'][u'items'][0]
   self.send_message(t,yt+' http://youtu.be/{id} > {title} | {views} | {rating}% | {duration}'.format(id=jd[u'id'],title=jd[u'title'].encode('utf-8','replace'),views="{:,}".format(jd[u'viewCount']),rating=str(jd[u'rating']/5*100).split('.')[0],duration=str(datetime.timedelta(seconds=jd[u'duration']))))
  except:
   self.send_message(t,yt+" No results :(")
   print "ERROR ",str(sys.exc_info())
   print traceback.print_tb(sys.exc_info()[2])
 if c in ['!g']:
  g=format.color('G',format.BLUE)+':'
  jd=json.load(urllib.urlopen('https://ajax.googleapis.com/ajax/services/search/web?v=1.0&q={q}'.format(q=urllib.quote_plus(p))))[u'responseData'][u'results'][0]
  self.send_message(t,g+' {title}: {url}'.format(title=jd[u'titleNoFormatting'].encode('utf-8','replace'),url=jd[u'unescapedUrl']))
 if len(c)>1 and c[0] in '!':
  cc=c[1:].lower()
  o={
  'ref'     :'!!bpm',
  'cah'     :'!!pyx',
  'steam':'http://steamsales.rhekua.com/ http://coustier.com/ http://steamgamesales.com/ http://steamprices.com/ http://steamregionalprices.com/',
  'ron':'Ron Paul: http://paul.house.gov/',
  'ftb':'Feed The Beast: http://forum.feed-the-beast.com/threads/ftb-release.736/',
  'rmod'    :'http://www.reddit.com/help/faq#Whatifthemoderatorsarebad',
  'rcss'    :'http://www.reddit.com/help/moderation#Appearance',
  'bpm'     :'BetterPonyMotes: http://rainbow.mlas1.us/',
  'brycode' :'BerryPunch code: https://bitbucket.org/socialery/berrypunch/src',
  'loe'     :'Legends of Equestria: http://www.legendsofequestria.com/',
  'pyx'     :"Pretend You\'re Xyzzy: http://pyx.mlas1.us/",
  'irc'     :'IRC Clients: http://irssi.org/ http://weechat.org/ http://xchat.servx.ru/ http://hexchat.org/ http://mirc.com/',
  'deb'     :'http://packages.debian.org/search?keywords='+urllib.quote_plus(p),
  'coc'     :'Corruption of Champions: http://fenoxo.com/play/',
  'ykwtsad' :'http://www.youtube.com/watch?v=_VKWLC87Uzw#t=680s',
  }
  while o.has_key(cc) and o[cc][0:2]=='!!':cc=o[cc][2:]
  if o.has_key(cc):self.send_message(t,o[cc])
	
